﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Collections;

namespace uart_test_program
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private bool test_run_COM2_simulate2()
        {
            SerialPort spComPort;
            bool error = false;

            spComPort = new SerialPort("COM2", 9600, Parity.None, 8, StopBits.One);
            spComPort.ReadTimeout = 100;
            spComPort.WriteTimeout = 100;
            spComPort.Handshake = Handshake.None;
            spComPort.DtrEnable = false;

            try
            {
                spComPort.Open();
            }
            catch (UnauthorizedAccessException) { error = true; }
            catch (IOException) { error = true; }
            catch (ArgumentException) { error = true; }
            if (error)
            {
                MessageBox.Show("Could not open the COM2 port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible");
                return false;
            }

            if (spComPort.DsrHolding == true)   /* DSR low Fail */
            {
                spComPort.Close();
                return false;
            }

            spComPort.Close();
            return true;
        }

        private bool test_run_COM2_simulate1()
        {
            SerialPort spComPort;
            bool error = false;

            spComPort = new SerialPort("COM2", 9600, Parity.None, 8, StopBits.One);
            spComPort.ReadTimeout = 100;
            spComPort.WriteTimeout = 100;
            spComPort.Handshake = Handshake.None;
            spComPort.DtrEnable = true;
            try
            {
                spComPort.Open();
            }
            catch (UnauthorizedAccessException) { error = true; }
            catch (IOException) { error = true; }
            catch (ArgumentException) { error = true; }
            if (error)
            {
                MessageBox.Show("Could not open the COM2 port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible");
                return false;
            }

            if (spComPort.DsrHolding == true)   /* DSR low Success */
            {
                spComPort.Close();
                return true;
            }
            else
            {
                spComPort.Close();
                return false;
            }
        }

        private bool test_run_COM1_simulate1()  /* 只測試Tx與RX對接 */
        {
            SerialPort spComPort;
            bool error = false;
            spComPort = new SerialPort("COM1", 9600, Parity.None, 8, StopBits.One);
            spComPort.ReadTimeout = 100;
            spComPort.WriteTimeout = 100;

            spComPort.Handshake = Handshake.None;
            spComPort.RtsEnable = true;
     
            try
            {
                spComPort.Open();
            }
            catch (UnauthorizedAccessException) { error = true; }
            catch (IOException) { error = true; }
            catch (ArgumentException) { error = true; }
            if (error)
            {
                MessageBox.Show("Could not open the COM1 port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible");
                return false;
            }
                

            byte[] sendBuf = new byte[1];
            byte[] recvBuf = new byte[1];


            for (int i = 0; i < 10; i++)
            {
                sendBuf[0] = (byte)i;
                try
                {
                    spComPort.Write(sendBuf, 0, 1);

                    spComPort.Read(recvBuf, 0, 1);
                }
                catch (TimeoutException)
                {
                    spComPort.Close();
                    return false;

                }

                if (recvBuf[0] != sendBuf[0])
                {
                    spComPort.Close();
                    return false;
                }
            }

            spComPort.Close();
            return true;
        }

        private void Start()
        {
            if (test_run_COM1_simulate1() == false)
            {
                lblOutComPort1Message.Text = "Com  Port  1  Test        State:Fail";
                lblOutComPort1Message.ForeColor = Color.Red;
            }
            else
            {
                lblOutComPort1Message.Text = "Com  Port  1  Test        State:Pass";
                lblOutComPort1Message.ForeColor = Color.Green;
            }

            if (test_run_COM2_simulate2() == false || test_run_COM2_simulate1() == false)
            {
                lblOutComPort2Message.Text = "Com  Port  2  Test        State:Fail";
                lblOutComPort2Message.ForeColor = Color.Red;
            }
            else
            {
                lblOutComPort2Message.Text = "Com  Port  2  Test        State:Pass";
                lblOutComPort2Message.ForeColor = Color.Green;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread thread = new Thread(Start);
            thread.Start();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(Start);
            thread.Start();
            thread.Join();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

    }
}
