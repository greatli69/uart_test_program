﻿namespace uart_test_program
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblOutComPort1Message = new System.Windows.Forms.Label();
            this.lblOutComPort2Message = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblOutComPort1Message
            // 
            this.lblOutComPort1Message.Font = new System.Drawing.Font("PMingLiU", 18F, System.Drawing.FontStyle.Bold);
            this.lblOutComPort1Message.ForeColor = System.Drawing.Color.Blue;
            this.lblOutComPort1Message.Location = new System.Drawing.Point(12, 24);
            this.lblOutComPort1Message.Name = "lblOutComPort1Message";
            this.lblOutComPort1Message.Size = new System.Drawing.Size(428, 27);
            this.lblOutComPort1Message.TabIndex = 0;
            this.lblOutComPort1Message.Text = "Com  Port  1  Test        State:";
            this.lblOutComPort1Message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOutComPort2Message
            // 
            this.lblOutComPort2Message.Font = new System.Drawing.Font("PMingLiU", 18F, System.Drawing.FontStyle.Bold);
            this.lblOutComPort2Message.ForeColor = System.Drawing.Color.Blue;
            this.lblOutComPort2Message.Location = new System.Drawing.Point(12, 69);
            this.lblOutComPort2Message.Name = "lblOutComPort2Message";
            this.lblOutComPort2Message.Size = new System.Drawing.Size(428, 27);
            this.lblOutComPort2Message.TabIndex = 1;
            this.lblOutComPort2Message.Text = "Com  Port  2  Test        State:";
            this.lblOutComPort2Message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(81, 117);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(93, 33);
            this.btnTest.TabIndex = 2;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(278, 117);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(93, 33);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(461, 178);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.lblOutComPort2Message);
            this.Controls.Add(this.lblOutComPort1Message);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ComPortTest";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblOutComPort1Message;
        private System.Windows.Forms.Label lblOutComPort2Message;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnExit;

    }
}

